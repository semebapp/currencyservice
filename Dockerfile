# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /currencyservice

COPY requirements.txt requirements.txt
COPY supported_currencies.csv supported_currencies.csv
RUN pip3 install -r requirements.txt
ENV FLASK_APP "api.py"

COPY . .

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]