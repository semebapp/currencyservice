from flask import Flask
from flask_restful import reqparse, abort, Api, Resource, fields, marshal_with
import csv
import urllib.request
import json

app = Flask(__name__)
api = Api(app)
key = "cb27abbbdb985e4a7fed520b"
pair_conversion = "https://v6.exchangerate-api.com/v6/{}/pair/{}/{}/{}"
version='0.0.1'

currencyInfo = []

set_marshaller = {
    'code': fields.String,
    'name': fields.String,
    'country': fields.String
}


class Currency(Resource):
    def __init__(self, code, name, country):
        self.code = code
        self.name = name
        self.country = country


class CurrencyList(Resource):
    @marshal_with(set_marshaller)
    def get(self):
        with open('supported_currencies.csv', newline='\n', encoding="utf8") as csv_file:
            reader = csv.reader(csv_file, delimiter='\t')
            next(reader, None)
            for code, name, country in reader:
                currencyInfo.append(Currency(code, name, country))
        return currencyInfo


class CurrencyConvert(Resource):
    def get(self, origin, target, amount):
        conversion_result = urllib.request.urlopen(pair_conversion.format(key, origin, target, amount)).read()
        marshalled = json.loads(conversion_result)
        return marshalled['conversion_result']

class About(Resource):
    def get(self):
        return 'Version: {}'.format(version)

class Health(Resource):
    def get(self):
        return 'OK'


# Resources
api.add_resource(CurrencyList, '/currency/codes')
api.add_resource(CurrencyConvert, '/currency/convert/<origin>/<target>/amount/<amount>')
api.add_resource(About, '/currency/about')
api.add_resource(Health, '/currency/health')


if __name__ == '__main__':
    app.run(debug=True)
